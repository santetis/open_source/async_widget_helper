import 'dart:async';

import 'package:async_helper_mixin/async_helper_mixin.dart';
import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Async Widget Helper Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Async widget helper demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  StreamController<int> _periodicController;

  @override
  void initState() {
    super.initState();
    _periodicController = StreamController.broadcast()
      ..addStream(
        Stream.periodic(
          Duration(seconds: 1),
          (timePassed) => timePassed,
        ),
      );
  }

  @override
  void dispose() {
    super.dispose();
    _periodicController.close();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            MyFutureWidget(
              future: Future.delayed(Duration(seconds: 5), () => 2),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16),
              child: Divider(
                height: 3,
              ),
            ),
            MyStreamWidget(_periodicController.stream),
          ],
        ),
      ),
    );
  }
}

class MyStreamWidget extends StatelessWidget with StreamBuilderHelperMixin {
  final Stream<int> periodicStream;

  MyStreamWidget(this.periodicStream);

  @override
  Stream<Widget> buildStream(BuildContext context) {
    return periodicStream.map((t) => Text('$t elapsed'));
  }

  @override
  Widget get defaultWidget => Text('Waiting stream');
}

class MyFutureWidget extends StatelessWidget with FutureBuilderHelperMixin {
  final Future<int> future;

  MyFutureWidget({@required this.future});

  @override
  Future<Widget> buildFuture(BuildContext context) async {
    final value = await future;
    return Text('Future result is $value');
  }

  @override
  Widget get defaultWidget => Text('Waiting future result');
}
