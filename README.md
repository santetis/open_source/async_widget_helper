[![pipeline status](https://gitlab.com/santetis/open_source/async_helper_mixin/badges/master/pipeline.svg)](https://gitlab.com/santetis/open_source/async_helper_mixin/commits/master)
[![coverage report](https://gitlab.com/santetis/open_source/async_helper_mixin/badges/master/coverage.svg)](https://gitlab.com/santetis/open_source/async_helper_mixin/commits/master)

Async helper mixin provide a simpler and clean interface to use FutureBuilder and StreamBuilder.

see the example [here](https://gitlab.com/santetis/open_source/async_helper_mixin/blob/master/example/lib/main.dart)