import 'dart:async';
import 'package:flutter/widgets.dart';

@Deprecated('Use StreamBuilderHelperMixin instead')
mixin StreamHelperMixin {
  Widget get defaultWidget => Container();

  Stream<Widget> buildStream(BuildContext context);

  Widget build(BuildContext context) {
    return StreamBuilder<Widget>(
      initialData: defaultWidget,
      stream: buildStream(context),
      builder: (c, snapshot) {
        return snapshot.data;
      },
    );
  }
}

mixin StreamBuilderHelperMixin {
  Widget get defaultWidget => Container();

  Stream<Widget> buildStream(BuildContext context);

  Widget build(BuildContext context) {
    return StreamBuilder<Widget>(
      initialData: defaultWidget,
      stream: buildStream(context),
      builder: (c, snapshot) {
        return snapshot.data;
      },
    );
  }
}
