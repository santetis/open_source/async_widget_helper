import 'dart:async';
import 'package:flutter/widgets.dart';

@Deprecated('Use FutureBuilderHelperMixin instead')
mixin FutureHelperMixin {
  Widget get defaultWidget => Container();

  Future<Widget> buildFuture(BuildContext context);

  Widget build(BuildContext context) {
    return FutureBuilder<Widget>(
      initialData: defaultWidget,
      future: buildFuture(context),
      builder: (c, snapshot) {
        return snapshot.data;
      },
    );
  }
}

mixin FutureBuilderHelperMixin {
  Widget get defaultWidget => Container();

  Future<Widget> buildFuture(BuildContext context);

  Widget build(BuildContext context) {
    return FutureBuilder<Widget>(
      initialData: defaultWidget,
      future: buildFuture(context),
      builder: (c, snapshot) {
        return snapshot.data;
      },
    );
  }
}
