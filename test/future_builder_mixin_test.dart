import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:async_helper_mixin/async_helper_mixin.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  testWidgets('FutureBuilderHelperMixin', (WidgetTester tester) async {
    final completer = Completer<int>();
    final app = MaterialApp(
      home: Scaffold(
        body: _FutureTestWidget(
          future: completer.future,
        ),
      ),
    );
    await tester.pumpWidget(app);

    expect(find.byType(Container), findsOneWidget);

    completer.complete(1);

    await tester.pumpAndSettle();
    expect(find.text('Future result is 1'), findsOneWidget);
  });

  testWidgets('FutureBuilderHelperMixin with custom defaultWidget',
      (WidgetTester tester) async {
    final completer = Completer<int>();
    final app = MaterialApp(
      home: Scaffold(
        body: _FutureTestWidgetWithCustomDefaultWidget(
          future: completer.future,
        ),
      ),
    );
    await tester.pumpWidget(app);

    expect(find.text('Future doesn\'t completed yet'), findsOneWidget);

    completer.complete(1);

    await tester.pumpAndSettle();
    expect(find.text('Future result is 1'), findsOneWidget);
  });
}

class _FutureTestWidget extends StatelessWidget with FutureBuilderHelperMixin {
  final Future<int> future;

  _FutureTestWidget({@required this.future});

  @override
  Future<Widget> buildFuture(BuildContext context) async {
    final result = await future;
    return Text('Future result is $result');
  }
}

class _FutureTestWidgetWithCustomDefaultWidget extends StatelessWidget
    with FutureBuilderHelperMixin {
  final Future<int> future;

  _FutureTestWidgetWithCustomDefaultWidget({@required this.future});

  @override
  Widget get defaultWidget => Text('Future doesn\'t completed yet');

  @override
  Future<Widget> buildFuture(BuildContext context) async {
    final result = await future;
    return Text('Future result is $result');
  }
}
